FROM python:3.12-slim-bookworm

RUN apt update && apt install -y git

WORKDIR /app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
